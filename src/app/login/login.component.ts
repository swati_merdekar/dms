import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {Http,Headers, Response} from '@angular/http';
import {Router} from '@angular/router';
import { map } from 'rxjs/operators';
import { authGuard } from '../services/routingGuards/canActivateGuard/authGuard';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailFormControl:FormControl;
  passwordFormControl:FormControl;
  loading:boolean=false;
  err:boolean=false;
  constructor(public http:Http,public router:Router,public guard:authGuard) { }

  ngOnInit() {
    this.emailFormControl=new FormControl('',[Validators.required,Validators.email]);
    this.passwordFormControl=new FormControl('',[Validators.required,Validators.minLength(5)]);
  }

  onSubmit(){
    this.loading=true;
    this.err=false;
    var headers:Headers=new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('login',"email="+this.emailFormControl.value+"&password="+this.passwordFormControl.value,{headers:headers})
    .pipe(map((response:Response)=>response.json()))
    .subscribe(data=>{
      this.loading=false;
      this.guard.login(data,()=>{this.router.navigate(["search"])});
      console.log(data);
    },err=>{
      this.err=true;
      this.loading=false;
      console.log("Invalid");
    });
  }

}
