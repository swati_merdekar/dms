import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder} from '@angular/forms';
import { Http,Response } from '@angular/http';
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  fileUploadForm:FormGroup;
  fileList:any=[];
  formData:FormData=new FormData();
  hideCategories:boolean=true;
  catData:any={
    "id":1,
    "name":"root"
  };

  selectedCategory(event){
    this.catData=event;
  }

  constructor(public fb:FormBuilder,public http:Http) {
    this.fileUploadForm=this.fb.group({
      'description':[''],
      'customField':[''],
      'documentDate':[this.formatDate(new Date().toDateString())],
      'alertDate':[''],
    });
   }
   
   formatDate(date) {
    date=date.split("/").reverse().join("/");
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour=''+d.getHours(),
        mins=''+d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 10) hour = '0' + hour;
    if (mins.length < 10) mins = '0' + mins;
    console.log([year, month, day].join('-')+"T"+hour+":"+mins);
    return [year, month, day].join('-')+"T"+hour+":"+mins;
  } 

  submit(value:any){
    value.description=value.description.trim();
    value.alertDate=value.alertDate.trim();
    this.formData.append('description',value.description);
    this.formData.append('alert_dat',value.alertDate);
    this.formData.append('category_id',this.catData.id);
    this.formData.append('custom_tag',this.fileUploadForm.controls.customField.value);
    console.log(this.formData.getAll("file_name"));
    console.log(this.formData.getAll("alert_dat"));
    console.log(this.formData.getAll("category_id"));
    console.log(this.formData.getAll("description"));
    this.apiCall();
    this.formData=new FormData();
  }
  
  apiCall(){
    this.http.post("archival/api/v1.0/upload_doc",this.formData)
    .subscribe((val)=>{
      console.log(val);
    });
  }

  addFile(){

    console.log('called addFile');
    var a = document.createElement("input");
    a.setAttribute('type','file');
    //a.setAttribute('multiple','true');
    document.body.appendChild(a);
    var thisRef=this;
    a.onchange=function(){
    thisRef.fileList=[];
    thisRef.fileList=a.files;
    console.log(thisRef.fileList);
      if(thisRef.fileList.length>0){
        for(var i=0;i<thisRef.fileList.length;i++){
          thisRef.formData.append('file_name',thisRef.fileList[i]);
      console.log(thisRef.fileList);
    }
  }   
};
  a.click();
}


  ngOnInit() {
  }
  
}
