import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { authGuard } from '../services/routingGuards/canActivateGuard/authGuard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {
  navOpen:boolean=false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );  
    
  constructor(public router:Router, public guard:authGuard, private breakpointObserver: BreakpointObserver) {}
  logout(){
    this.guard.logout(()=>{
      this.router.navigate(["login"]);
    });
  }
  }
