import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SearchComponent} from './search/search.component';
import {SidenavComponent} from './sidenav/sidenav.component';
import {SearchResultsComponent} from './search-results/search-results.component';
import {EditTableDataComponent} from './edit-table-data/edit-table-data.component';
import {ActionsComponent} from './actions/actions.component';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {BulkUploadComponent} from './bulk-upload/bulk-upload.component';
import {TestComponent} from './test/test.component';
import {DocumentViewerComponent} from './document-viewer/document-viewer.component';
import { authGuard,loginAuthGuard } from './services/routingGuards/canActivateGuard/authGuard';
const routes: Routes = [
    {path:'login',component:LoginComponent,canActivate:[loginAuthGuard]},
    {path:'search',component:SearchComponent,canActivate:[authGuard]},
    {path:'sidenav',component:SidenavComponent,canActivate:[authGuard]},
    {path:'results/:categoryId',component:SearchResultsComponent,canActivate:[authGuard]},
    {path:'editTableData',component:EditTableDataComponent,canActivate:[authGuard]},
    {path:'actions/:docId',component:ActionsComponent,canActivate:[authGuard]},
    {path:'fileUpload',component:FileUploadComponent,canActivate:[authGuard]},
    {path:'bulkUpload',component:BulkUploadComponent,canActivate:[authGuard]},
    {path:'test',component:TestComponent,canActivate:[authGuard]},
    {path:'documentView',component:DocumentViewerComponent,canActivate:[authGuard]},
    {path:'',redirectTo:"search",pathMatch:'full'},
    {path:"**",redirectTo:"login"}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
