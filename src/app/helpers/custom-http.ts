import { Injectable, Inject } from "@angular/core";
import { ConnectionBackend, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from "@angular/http";
import { Observable,throwError } from "rxjs";
import { catchError} from 'rxjs/operators';
var host=window.location.hostname;
@Injectable()
export class CustomHttp extends Http {
    BASE_URL="http://demo1.yukthi.biz:8084/";
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        console.log(this.BASE_URL + url);
        return super.get(this.BASE_URL + url, this.addJwt(options)).pipe(catchError(this.handleError));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        console.log(this.BASE_URL + url);
        return super.post(this.BASE_URL + url, body, this.addJwt(options)).pipe(catchError(this.handleError));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        console.log(this.BASE_URL + url);
        return super.put(this.BASE_URL + url, body, this.addJwt(options)).pipe(catchError(this.handleError));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        console.log(this.BASE_URL + url);
        return super.delete(this.BASE_URL + url, this.addJwt(options)).pipe(catchError(this.handleError));
    }

    // private helper methods

    private addJwt(options?: RequestOptionsArgs): RequestOptionsArgs {
        
        // ensure request options and headers are not null
        options = options || new RequestOptions();
        options.headers = options.headers || new Headers();
        // add authorization header with jwt token
        //options.headers.append('Content-Type', 'application/x-www-form-urlencoded');
     
        return options;
    }

    private handleError(error: any) {
        if (error.status === 401) {
            // 401 unauthorized response so log user out of client
              // window.location.href = '/login';
            localStorage.removeItem("currentUser");
        }
        return throwError(error._body);
    }
}

export function customHttpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
    return new CustomHttp(xhrBackend, requestOptions);
}

export let customHttpProvider = {
    provide: Http,
    useFactory: customHttpFactory,
    deps: [XHRBackend, RequestOptions]
};
export const BASE_URL="http://demo1.yukthi.biz:8084/";
