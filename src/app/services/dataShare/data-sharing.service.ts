import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataSharingService {
data:BehaviorSubject<any>;
  constructor() { 
    this.data=new BehaviorSubject<any>(null);
  }
  getObservable():Observable<any>{
    return this.data.asObservable();
  }
  setData(data:any){
    this.data.next(data);
  }
}
