import { TestBed, inject } from '@angular/core/testing';

import { ActionServiceService } from './action-service.service';

describe('ActionServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionServiceService]
    });
  });

  it('should be created', inject([ActionServiceService], (service: ActionServiceService) => {
    expect(service).toBeTruthy();
  }));
});
