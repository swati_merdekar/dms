import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor(public http:Http) { }

  addUpdate(docId:number,remarks:string){
    return new Promise((res,rej)=>{
      console.log("called Promise");
    this.http.post('archival/api/v1.0/archival_docs/'+docId+'/updates',{remarks:remarks})
    .subscribe((val)=>{
        console.log(val);
        res(val);
    },err=>{
      rej("error");
    });
  });
  }
checkOut(docId){
  return new Promise((res,rej)=>{
    console.log("called Promise");
  this.http.put('archival/api/v1.0/archival_docs/'+docId+'/checkout1',"")
  .subscribe((val)=>{
      res(val);
    },err=>{
    rej("error");
  });
  });
}
checkIn(docId,data){
  return new Promise((res,rej)=>{
    console.log("called",docId,data.get("file_nm"));
  this.http.post('archival/api/v1.0/archival_docs/'+docId+'/versions',data)
  .pipe(map((response:Response)=>response.json()))
  .subscribe((val)=>{
      console.log(val);
      res(val);
  },err=>{
    rej("error");
  });
  });
}

downloadLog(docId:number){
  return new Promise((res,rej)=>{
    console.log("called Promise");
  this.http.get('admin/api/v1.0/archival_docs/'+docId+'/download_logs')
  .pipe(map((response:Response)=>response.json()))
  .subscribe((val)=>{
      console.log(val);
      res(val);
  },err=>{
    rej("error");
  });
  });
  }
startWorkflow(){

}
getDownloadLink(docId){
  return new Promise((res,rej)=>{
    console.log("called Promise");
  this.http.post('archival/api/v1.0/archival_docs/'+docId+'/url',{})
  .pipe(map((response:Response)=>response.json()))
  .subscribe((val)=>{
      console.log(val);
      res(val);
  },err=>{
    rej("error");
  });
  });
}
}
