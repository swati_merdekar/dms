import {CanActivate, Router} from "@angular/router";
import {Injectable} from '@angular/core';
@Injectable()
export class authGuard implements CanActivate{
    constructor(private router:Router){

    }
    login(data,callback){
        sessionStorage.setItem("user",data);
        callback();
    }
    logout(callback){
        sessionStorage.removeItem("user");
        sessionStorage.removeItem("resultData");
        sessionStorage.removeItem("searchParam");
        callback();
        console.log("logout called");
    }
    canActivate():boolean{
        if(sessionStorage.getItem('user')!=null)
        return true;
        else{
            this.router.navigate(["login"]);
            return false;
        }
    }
}
@Injectable()
export class loginAuthGuard implements CanActivate{
    constructor(public router:Router){

    }
    canActivate():boolean{
        if(sessionStorage.getItem('user')==null)
        return true;
        else{
            this.router.navigate(["search"]);
            return false;
        }
    }
}