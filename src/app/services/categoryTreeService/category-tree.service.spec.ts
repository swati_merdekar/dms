import { TestBed, inject } from '@angular/core/testing';

import { CategoryTreeService } from './category-tree.service';

describe('CategoryTreeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoryTreeService]
    });
  });

  it('should be created', inject([CategoryTreeService], (service: CategoryTreeService) => {
    expect(service).toBeTruthy();
  }));
});
