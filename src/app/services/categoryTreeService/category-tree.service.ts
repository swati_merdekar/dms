import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CategoryTreeService {

  constructor(public http:Http) {
    this.callAPI();
  }

  
  callAPI(){
    return new Promise(resolve=>
    { 
         var string="";
    this.http.get("admin/api/v1.0/categories")
    .pipe(map((response:Response)=>response.json()))
    .subscribe((val)=>{
        resolve(val);
    });
    });
  }

   buildNestedObject(data:any[]){
    return JSON.stringify(this.convertToObject(this.getNestedChildren(data,null)));
  }
  private getNestedChildren(arr, parent) {
    var out = []
    for(var i in arr) {
        if(arr[i].parent_id == parent) {
            var children = this.getNestedChildren(arr, arr[i].id)

            if(children.length) {
                arr[i].children = children
            }
            out.push(arr[i])
        }
    }
    return out;
  }
  private convertToObject(arr){
    var obj={};
    arr.map((val,index)=>{
      console.log(val);

      if(val.hasOwnProperty("children"))
      {
        obj[val.name+"&&id="+val.id]=this.convertToObject(val.children);        
      }
      else{
        obj[val.name+"&&id="+val.id]=val.id;
      }
    },this);
    return obj;
  }

   getPath(arr,id){
    var path=[];
    var path_info:any[]=arr.filter(data=>data.id==id)[0].path_info;
    if(path_info!=undefined){
    return new Promise(resolve=>{
      path_info.map((val,index)=>{
        path.push(arr.filter(data=>data.id==val)[0].name);
        if(index==path_info.length-1)
        {
          resolve ({"filename":path.pop(),"path":path.join(">")});
        }
      });
    })
    }
    }

}
