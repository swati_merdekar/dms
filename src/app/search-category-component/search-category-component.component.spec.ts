import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCategoryComponentComponent } from './search-category-component.component';

describe('SearchCategoryComponentComponent', () => {
  let component: SearchCategoryComponentComponent;
  let fixture: ComponentFixture<SearchCategoryComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchCategoryComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCategoryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
