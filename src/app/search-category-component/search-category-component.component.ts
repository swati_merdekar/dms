import {FlatTreeControl} from '@angular/cdk/tree';
import {Component, Injectable, Output,EventEmitter} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject, Observable, of as observableOf} from 'rxjs';
import {CategoryTreeService} from '../services/categoryTreeService/category-tree.service';

/**
 * File node data with nested structure.
 * Each node has a filename, and a type or a list of children.
 */
export class FileNode {
  children: FileNode[];
  filename: string;
  type: any;
}

/** Flat node with expandable and level information */
export class FileFlatNode {
  constructor(
    public expandable: boolean, public filename: string, public level: number, public type: any) {}
}

/**
 * The file structure tree data in string. The data could be parsed into a Json object
 */
var TREE_DATA = JSON.stringify({
  Applications: {
    Calendar: 'app',
    Chrome: 'app',
    Webstorm: 'app'
  },
  Documents: {
    angular: {
      src: {
        compiler: 'ts',
        core: 'ts'
      }
    },
    material2: {
      src: {
        button: 'ts',
        checkbox: 'ts',
        input: 'ts'
      }
    }
  },
  Downloads: {
    October: 'pdf',
    November: 'pdf',
    Tutorial: 'html'
  },
  Pictures: {
    'Photo Booth Library': {
      Contents: 'dir',
      Pictures: 'dir'
    },
    Sun: 'png',
    Woods: 'jpg'
  }
});

/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has filename and type.
 * For a directory, it has filename and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */
@Injectable()
export class FileDatabase {
  dataChange = new BehaviorSubject<FileNode[]>([]);
  testData:any={
    "cat_list": [
      {
        "dept_id": null, 
        "id": 1, 
        "name": "root", 
        "parent_id": null, 
        "path_info": [
          1
        ]
      }, 
      {
        "dept_id": null, 
        "id": 2, 
        "name": "Top_level", 
        "parent_id": 1, 
        "path_info": [
          1, 
          2
        ]
      }, 
      {
        "dept_id": null, 
        "id": 3, 
        "name": "Rt_Docs", 
        "parent_id": 1, 
        "path_info": [
          1, 
          3
        ]
      }, 
      {
        "dept_id": null, 
        "id": 4, 
        "name": "Top Level  Sub A", 
        "parent_id": 2, 
        "path_info": [
          1, 
          2, 
          4
        ]
      }, 
      {
        "dept_id": null, 
        "id": 5, 
        "name": "Top Level Sub B", 
        "parent_id": 2, 
        "path_info": [
          1, 
          2, 
          5
        ]
      }, 
      {
        "dept_id": null, 
        "id": 6, 
        "name": "Sub Level Bb", 
        "parent_id": 5, 
        "path_info": [
          1, 
          2, 
          5, 
          6
        ]
      }, 
      {
        "dept_id": null, 
        "id": 7, 
        "name": "Bb2", 
        "parent_id": 5, 
        "path_info": [
          1, 
          2, 
          5, 
          7
        ]
      }, 
      {
        "dept_id": null, 
        "id": 8, 
        "name": "Aa1", 
        "parent_id": 4, 
        "path_info": [
          1, 
          2, 
          4, 
          8
        ]
      }
    ]
  };    
  get data(): FileNode[] { return this.dataChange.value; }

  constructor(public buildNestedObject:CategoryTreeService) {
    this.buildNestedObject.callAPI().then((val)=>{
      TREE_DATA=this.buildNestedObject.buildNestedObject(val["cat_list"]);
      this.buildNestedObject.getPath(val["cat_list"],8);
      this.initialize();
    });
   
  }
  getNestedChildren(arr, parent) {
    var out = []
    for(var i in arr) {
        if(arr[i].parent_id == parent) {
            var children = this.getNestedChildren(arr, arr[i].id)

            if(children.length) {
                arr[i].children = children
            }
            out.push(arr[i])
        }
    }
    return out;
  }
  convertToObject(arr){
    console.log
    var obj={};
    arr.map((val,index)=>{
      console.log(val);

      if(val.hasOwnProperty("children"))
      {
        obj[val.name]=this.convertToObject(val.children);        
      }
      else{
        obj[val.name]={};
      }
    },this);
    return obj;
  }


  initialize() {
    // Parse the string to json object.
    const dataObject = JSON.parse(TREE_DATA);

    // Build the tree nodes from Json object. The result is a list of `FileNode` with nested
    //     file node as children.
    const data = this.buildFileTree(dataObject, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `FileNode`.
   */
  buildFileTree(obj: object, level: number): FileNode[] {
    return Object.keys(obj).reduce<FileNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new FileNode();
      node.filename = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.type = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'app-search-category-component',
  templateUrl: './search-category-component.component.html',
  styleUrls: ['./search-category-component.component.css'],
  providers:[FileDatabase]
})
export class SearchCategoryComponentComponent {
  treeControl: FlatTreeControl<FileFlatNode>;
  treeFlattener: MatTreeFlattener<FileNode, FileFlatNode>;
  dataSource: MatTreeFlatDataSource<FileNode, FileFlatNode>;
  selectedCategory:any={
    id:'1',
    name:'root'
  }
  @Output() categorySelected=new EventEmitter<any>();
  constructor(database: FileDatabase) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
      this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<FileFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    database.dataChange.subscribe(data => this.dataSource.data = data);
  }


  selectCategory(node:any){
    this.selectedCategory.id=node.filename.split("&&id=")[1];
    this.selectedCategory.name=node.filename.split("&&id=")[0];
    this.categorySelected.emit(this.selectedCategory);
    console.log(this.selectedCategory);
  }

  transformer = (node: FileNode, level: number) => {
    return new FileFlatNode(!!node.children, node.filename, level, node.type);
  }

  private _getLevel = (node: FileFlatNode) => node.level;

  private _isExpandable = (node: FileFlatNode) => node.expandable;

  private _getChildren = (node: FileNode): Observable<FileNode[]> => observableOf(node.children);

  hasChild = (_: number, _nodeData: FileFlatNode) => _nodeData.expandable;
}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
