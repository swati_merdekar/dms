import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatButtonModule,MatPaginatorModule,MatIconModule, MatCheckboxModule,MatInputModule,MatFormFieldModule, MatFormFieldControl, MatToolbarModule, MatListModule, MatSelectModule, MatSnackBarModule, MatProgressBarModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {customHttpProvider} from './helpers/index';
import {HttpModule} from '@angular/http';
import { SearchComponent } from './search/search.component';
import {MatTreeModule} from '@angular/material/tree';
import { SearchCategoryComponentComponent } from './search-category-component/search-category-component.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LayoutModule } from '@angular/cdk/layout';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import { SearchResultsComponent } from './search-results/search-results.component';
import { ResultsTableComponent } from './results-table/results-table.component';
import { EditTableDataComponent } from './edit-table-data/edit-table-data.component';
import {DataSharingService} from './services/dataShare/data-sharing.service';
import { ActionsComponent } from './actions/actions.component';
import { UpdateTableComponent } from './actionTables/update-table/update-table.component';
import { SubDocumentTableComponent } from './actionTables/sub-document-table/sub-document-table.component';
import { VersionsTableComponent } from './actionTables/versions-table/versions-table.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { TestComponent } from './test/test.component';
import { CategoryDisplayPipePipe } from './pipes/categoryDisplayPipe/category-display-pipe.pipe';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { DomSanitizerPipe } from './pipes//domSanitizerPipe/dom-sanitizer.pipe';
import {ActionService} from './services/actionsService/action-service.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DownloadLogTableComponent } from './actionTables/download-log-table/download-log-table.component';
import { BulkUploadTableComponent } from './bulk-upload/bulk-upload-table/bulk-upload-table.component';
import {authGuard, loginAuthGuard} from './services/routingGuards/canActivateGuard/authGuard';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SearchComponent,
    SearchCategoryComponentComponent,
    SidenavComponent,
    SearchResultsComponent,
    ResultsTableComponent,
    EditTableDataComponent,
    ActionsComponent,
    UpdateTableComponent,
    SubDocumentTableComponent,
    VersionsTableComponent,
    FileUploadComponent,
    BulkUploadComponent,
    TestComponent,
    CategoryDisplayPipePipe,
    DocumentViewerComponent,
    DomSanitizerPipe,
    DownloadLogTableComponent,
    BulkUploadTableComponent
    ],
  imports: [
    NgbModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatCardModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTreeModule,
    MatSidenavModule,
    MatTableModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatListModule
  ],
  providers: [
    loginAuthGuard,
    authGuard,
    customHttpProvider,
    DataSharingService,
    ActionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
