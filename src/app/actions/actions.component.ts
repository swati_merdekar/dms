import { Component, OnInit, ViewChild, TemplateRef, ComponentRef, AfterViewInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import {ActionService} from '../services/actionsService/action-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';
import { UpdateTableComponent } from '../actionTables/update-table/update-table.component';
import { MatSnackBar } from '@angular/material';
import {BASE_URL} from '../helpers/custom-http';
import { VersionsTableComponent } from '../actionTables/versions-table/versions-table.component';
@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  filename:string;
  workflowStage:string;
  displayActions:boolean=true;
  selectedRow:any;
  comment:FormControl;
  docId:number=0;
  fileList:any=[];
  formData:FormData;
  versionNumber:FormControl;
  @ViewChild('versionTable') versionTable:VersionsTableComponent;
  @ViewChild('updateTable') updateTable:UpdateTableComponent;
  @ViewChild('modalContent3') downloadLog: TemplateRef<any>;
  @ViewChild('modalContent2') versionCheckIn: TemplateRef<any>;
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  constructor(private snackBar:MatSnackBar, private modal: NgbModal,public actions:ActionService,public location:Location,public route:ActivatedRoute) { 
    this.route.params.subscribe((val)=>{
      this.docId=val.docId;
    })
    this.filename="Login.jsx";
    this.workflowStage="None";
    this.formData=new FormData();
  }

  callbackActionCheckInVersion(){
    this.formData.append("version_number",this.versionNumber.value);
    this.actions.checkIn(this.docId,this.formData).then(()=>{
      this.versionTable.getData();
      this.versionNumber.reset();
      this.snackBarOpen("Document checked in successfully");
      this.formData=new FormData();
      this.fileList=[];
    }).catch(()=>{
      this.versionNumber.reset();
      this.snackBarOpen("Error checking in.Try again");
    });
  }


  callbackActionAddUpdate(){
    this.actions.addUpdate(this.docId,this.comment.value)
    .then(()=>{
      console.log("test");
      this.updateTable.getData();
      this.comment.reset();
      this.snackBarOpen("Update added successfully");
    })
    .catch(err=>{
      this.comment.reset();
      this.snackBarOpen("Update failed.Try again");
    });
  }

  callBackDownloadLink(){
    this.actions.getDownloadLink(this.docId)
    .then((val)=>{
      window.alert(BASE_URL+val["message"]);
    }).catch(()=>{
      this.snackBarOpen("Generating link failed.Try again");
    });
  }

  callBackCheckOut(){
    this.actions.checkOut(this.docId)
    .then((val)=>{
      console.log(val);
      window.open(BASE_URL+'archival/api/v1.0/archival_docs/'+this.docId);  
    })
    .catch(()=>{
      this.snackBarOpen("Checkout failed.It may have already been checked out");
    });
  }

  ngOnInit() {
    this.comment=new FormControl('',[Validators.required]);
    this.versionNumber=new FormControl('',[Validators.required]);
    this.route.params.subscribe((val)=>{
      this.selectedRow=JSON.parse(sessionStorage.getItem("resultData")).filter(data=>data.id==val.docId)[0];
      if(this.selectedRow.file_name!=null)
      this.filename=this.selectedRow.file_name;
      if(this.selectedRow.workflow_stage!=null)
      this.workflowStage=this.selectedRow.workflow_stage;
    });
  }

  
  addFile(){
    console.log('called addFile');
    var a = document.createElement("input");
    a.setAttribute('type','file');
    a.setAttribute('multiple','true');
    document.body.appendChild(a);
    var thisRef=this;
    a.onchange=function(){
    thisRef.fileList=[];
    thisRef.fileList=a.files;
    console.log(thisRef.fileList);
      if(thisRef.fileList.length>0){
        for(var i=0;i<thisRef.fileList.length;i++){
        thisRef.formData.append('file_nm',thisRef.fileList[i]);
      console.log(thisRef.fileList);
    }
  }   
};
  a.click();
}

snackBarOpen(message:string){
  this.snackBar.open(message,"",{
    duration:2000
  });
}

}
