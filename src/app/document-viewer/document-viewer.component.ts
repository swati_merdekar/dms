import { Component, OnInit } from '@angular/core';
import {DataSharingService} from '../services/dataShare/data-sharing.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Http,Response } from '@angular/http';
import { map } from 'rxjs/operators';
import {Location} from '@angular/common';
@Component({
  selector: 'app-document-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.component.css']
})
export class DocumentViewerComponent implements OnInit {

  constructor(public location:Location,public http:Http, public domsanitizer:DomSanitizer,public dataShare:DataSharingService) { }
  htmlData:any;
  ngOnInit() {
    this.dataShare.getObservable()
    .subscribe((data)=>{      
      if(data==null)
      this.location.back();
   //   window.open(data);
      this.htmlData=data;
      console.log(this.htmlData);
    });
  }
}
