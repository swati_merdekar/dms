import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CategoryTreeService} from '../services/categoryTreeService/category-tree.service'
import {Http, Headers,Response} from '@angular/http'
import { map } from 'rxjs/operators';
import { MatSnackBarModule, MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  categoryId:number;
  dataSource:any;
  loading:any=false;
  loadingzip:any=false;
  zipString:any=[];
  pathDetails:any={filename:'',path:''};
  constructor(public snackBar:MatSnackBar,public http:Http,public buildNestedObject:CategoryTreeService, public route:ActivatedRoute) { 
    this.dataSource=JSON.parse(sessionStorage.getItem("resultData")); 
    this.dataSource.map((val,index)=>{
      this.zipString.push(val.id);
    });
    this.route.params.subscribe((val)=>{
    if(val.categoryId!='none')
    {
      this.categoryId=val.categoryId;
      this.buildNestedObject.callAPI().then((val)=>{
        this.buildNestedObject.getPath(val["cat_list"],this.categoryId).then((val)=>{
          this.pathDetails=val;
        });
      });
    } 
    })
  }

  downloadZip(){
    this.loadingzip=true;
    var headers:Headers=new Headers();
    console.log(this.zipString);
    headers.append("Content-type","application/json");
    headers.append('zipstring',this.zipString.join(","));
    this.http.get('archival/api/v1.0/archival_docs/zip_download',{headers:headers})
    .pipe(map((response:Response)=>response.json()))
    .subscribe((val)=>{
      this.loadingzip=false;
      window.open("http://demo1.yukthi.biz:8084"+val["message"]);
    },err=>{
      this.loadingzip=false;
      this.snackbarOpen("Failed to download file.Try again");
    });
  }

  downloadReport(){
    this.loading=true;
    var headers:Headers=new Headers();
    headers.append("Content-type","application/json");
    headers.append('zipstring','1,2,3,12');
    this.http.get('archival/api/v1.0/get_file_metadata',{headers:headers})
    .subscribe((val)=>{
      this.downloadFileId(val["_body"],"text/plain","report.txt");
    },err=>{
      this.loading=false;
      this.snackbarOpen("Failed to download file.Try again");
    });
  }

  downloadFileId(data: string , fileType:string , fileName:string){
    /* var blob = new Blob([data], { type: 'text/csv' });
     var url= window.URL.createObjectURL(blob);
     window.open(url,'report.txt');
   */
         var a = document.createElement("a");
         document.body.appendChild(a);
         var blob = new Blob([data],{type:fileType});
         console.log(blob);
         var url= window.URL.createObjectURL(blob);
         a.href=url;
         a.download=fileName;
         this.loading=false;
         a.click();
         window.URL.revokeObjectURL(url);
   }

  ngOnInit() {
  }

  snackbarOpen(message){
    this.snackBar.open(message,"",{
      duration:2000
    });
  }
}
