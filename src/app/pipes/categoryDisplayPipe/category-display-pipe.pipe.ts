import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryDisplayPipe'
})
export class CategoryDisplayPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.split("&&id=")[0];
  }

}
