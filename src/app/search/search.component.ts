import {FlatTreeControl} from '@angular/cdk/tree';
import {Component, Injectable} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject, Observable, of as observableOf} from 'rxjs';
import {FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl} from '@angular/forms';
import {Http, Headers} from '@angular/http';
import { map } from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {DataSharingService} from '../services/dataShare/data-sharing.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
formData:FormData=new FormData();
selectedCategoryData:any={
  name:"root",
  id:1
};
selected:string="eq_doc_date";
searchForm:FormGroup;
  constructor(public router:Router,public dataShare:DataSharingService,public snackBar:MatSnackBar,public fb:FormBuilder,public http:Http){
  this.searchForm=this.fb.group({
    description:[''/*,[Validators.required,Validators.minLength(5)]*/],
    date:[this.formatDate(new Date().toDateString()),[Validators.required/*,ForbiddenNameValidator()*/]],
    customField:['']
  });
}

selectedCategory(event){
  this.selectedCategoryData=event;
}

onSubmit(value:any,useCategories:boolean){
  this.genPostData(value,useCategories,(postData)=>{
  var headers=new Headers();
  headers.append('maxpage','1000');
  headers.append('Content-Type', 'application/x-www-form-urlencoded');
  //headers.append('pageno','1');
  this.http.post("archival/api/v1.0/archival_docs_page2",postData,{headers:headers})
  .pipe(map((response:any)=>response.json()))
  .subscribe((res)=>{
    sessionStorage.setItem("searchParam",JSON.stringify({postData:postData,headers:{headers:headers}}));
    console.log(res);
    sessionStorage.setItem("resultData",JSON.stringify(res["docs"]));
    this.searchForm.reset();
    this.formData=new FormData();
    this.selectedCategoryData={
      name:"root",
      id:1
    };
    this.selected="eq_doc_date";
    if(useCategories)
    this.router.navigate(["results",this.selectedCategoryData.id]);
    else
    this.router.navigate(["results","none"]);
  },err=>{
    this.snackBar.open("No documents found","",{
      duration:2000
    })
  });
  });
}

genPostData(value,useCategories,callback){
  console.log(value);
var bodyString=[];
  if( value.description!="undefined" && value.description!=null && value.description.trim()!="")
  bodyString.push("description="+value.description);
  if(useCategories && this.selectedCategoryData.name.trim()!=undefined && this.selectedCategoryData.name.trim()!="" && this.selectedCategoryData.name.trim()!=null )
  bodyString.push("category_id="+this.selectedCategoryData.id);
  if(value.customField.trim()!=undefined && value.customField.trim()!=null && value.customField.trim()!="")
  bodyString.push("custom_tag="+value.customField);
  bodyString.push(this.selected+value.date);
  callback(bodyString.join("&"));
  //callback(this.formData);
}

formatDate(date) {
  date=date.split("/").reverse().join("/");
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  console.log([year, month, day].join('-'));
  return [year, month, day].join('-');
} 
test(){
  console.log(this.searchForm.controls.description.hasError('required'));
  console.log(this.searchForm.controls.date);
}
}

function ForbiddenNameValidator():ValidatorFn{
  return (control:AbstractControl):{[key:string]:any} | null=>{
    return new Date(control.value).getTime()>new Date().getTime()?{'invalidDate':{value:control.value}}:null;
  }
}


