import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadTableComponent } from './bulk-upload-table.component';

describe('BulkUploadTableComponent', () => {
  let component: BulkUploadTableComponent;
  let fixture: ComponentFixture<BulkUploadTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkUploadTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
