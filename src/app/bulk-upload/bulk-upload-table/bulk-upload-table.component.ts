import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-bulk-upload-table',
  templateUrl: './bulk-upload-table.component.html',
  styleUrls: ['./bulk-upload-table.component.css']
})
export class BulkUploadTableComponent implements OnInit {
  @Input('dataSource') inputDataSource:Observable<any>;
  dataSource:any;
  displayedColumns:string[]=['file_name','imported','uploadedBy'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor() { }


  ngOnInit() {
    this.inputDataSource.subscribe((val)=>{
      console.log(val);
      if(val!=null){
      this.dataSource=val['doc_list'];
      this.dataSource=new MatTableDataSource<any>(this.dataSource);
      this.dataSource.paginator=this.paginator;
      }  
    })
  }

}
