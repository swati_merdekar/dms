import { Component, OnInit } from '@angular/core';
import { Http,Headers, Response } from '@angular/http';
import {BehaviorSubject} from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.css']
})
export class BulkUploadComponent implements OnInit {
  dataSource:any;
  fileList:any=[];
  existingxls:any=new BehaviorSubject<any>([]);
  displayedColumns:string[]=['name','size'];
  formData:FormData=new FormData();
  constructor(public snackBar:MatSnackBar, public router:Router, public http:Http) {
    this.existingFiles();
   }

  existingFiles(){
    this.http.get("archival/api/v1.0/metadata_files")
    .pipe(map((response:Response)=>response.json()))
    .subscribe((res)=>{
      console.log(res);
      this.existingxls.next(res);
    },err=>{
      this.snackBarOpen("Could'nt get metadata list");
    });
  }

  returnObservable(){
    return this.existingxls.asObservable();
  }

  ngOnInit() {
    this.dataSource=[]; 
  }
  submit(){
    this.http.post("archival/api/v1.0/metadata_files",this.formData)
    .subscribe((val)=>{
      console.log(val);
      this.formData=new FormData();
      this.existingFiles();
      this.snackBarOpen("File uploaded successfully");
    },err=>{
      this.snackBarOpen("File upload failed");
      console.log(err);
    });
  }

  snackBarOpen(message:string){
    this.snackBar.open(message,"",{
      duration:2000
    });
  }

  addFile(){
    console.log('called addFile');
    var a = document.createElement("input");
    a.setAttribute('type','file');
    //a.setAttribute('multiple','true');
    document.body.appendChild(a);
    var thisRef=this;
    a.onchange=function(){
    thisRef.fileList=[];
    thisRef.fileList=a.files;
    console.log(thisRef.fileList);
      if(thisRef.fileList.length>0){
        thisRef.dataSource=[];
        for(var i=0;i<thisRef.fileList.length;i++){
         thisRef.formData.append('metadata_file',thisRef.fileList[i]);
        }
  }   
};
  a.click();
}
}
