import { Component, OnInit, ViewChild, Input, OnChanges } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import {Router} from '@angular/router';
import {DataSharingService} from '../services/dataShare/data-sharing.service';
import {Http} from '@angular/http';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-results-table',
  templateUrl: './results-table.component.html',
  styleUrls: ['./results-table.component.css']
})
export class ResultsTableComponent implements OnInit,OnChanges {
  loading:boolean=false;
  dataSource:any;
  displayedColumns:string[]=["filename","category","description","customField","alertDate","byDate","docDate","Actions"];
  @Input() categoryId:number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public snackBar:MatSnackBar, public http:Http,public router:Router,public dataShare:DataSharingService) { 
   
  }
  ngOnChanges(){ 
    this.refreshData();
   }
  refreshData(){
    this.loading=true;
    console.log("ngOnchange called");
    var httpData=JSON.parse(sessionStorage.getItem("searchParam"));
    this.http.post("archival/api/v1.0/archival_docs_page2",httpData.postData,httpData.headers)
  .pipe(map((response:any)=>response.json()))
  .subscribe((res)=>{
    sessionStorage.setItem("searchParam",JSON.stringify({postData:httpData.postData,headers:httpData.headers}));
    console.log(res);
    sessionStorage.setItem("resultData",JSON.stringify(res["docs"]));
    this.dataSource=JSON.parse(sessionStorage.getItem("resultData")); 
    this.dataSource = new MatTableDataSource<any>(this.dataSource);
    this.dataSource.paginator=this.paginator;
    this.loading=false;
  },err=>{
    this.loading=false;
    this.snackBar.open("No documents found","",{
      duration:2000
    })
  });
  }
  displayFile(row){
    console.log(row);
/*    window.open("http://docs.google.com/gview?url=http://demo1.yukthi.biz:8084/archival/api/v1.0/archival_docs/"+row.id+"&embedded=true");
    //window.location.href="http://demo1.yukthi.biz:8084/archival/api/v1.0/archival_docs);
    this.http.get('archival/api/v1.0/archival_docs/'+row.id)
    .subscribe((data)=>{
      console.log(data);
    });*/
    this.dataShare.setData("http://demo1.yukthi.biz:8084/archival/api/v1.0/archival_docs/"+row.id+"/view2");
    this.router.navigate(["documentView"]);
  }
  callActions(element){
    console.log(element);
    this.router.navigate(["actions",element.id]);
  }
  init(){
  }

  clicked(row){
    console.log(row);
    this.dataShare.setData(row);
    this.router.navigate(["editTableData"]);
  }

  ngOnInit() {
    this.init();
  }

}
