import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  tempObject:any={};
  test:any={
    "cat_list": [
      {
        "dept_id": null, 
        "id": 1, 
        "name": "root", 
        "parent_id": null, 
        "path_info": [
          1
        ]
      }, 
      {
        "dept_id": null, 
        "id": 2, 
        "name": "Top_level", 
        "parent_id": 1, 
        "path_info": [
          1, 
          2
        ]
      }, 
      {
        "dept_id": null, 
        "id": 3, 
        "name": "Rt_Docs", 
        "parent_id": 1, 
        "path_info": [
          1, 
          3
        ]
      }, 
      {
        "dept_id": null, 
        "id": 4, 
        "name": "Top Level  Sub A", 
        "parent_id": 2, 
        "path_info": [
          1, 
          2, 
          4
        ]
      }, 
      {
        "dept_id": null, 
        "id": 5, 
        "name": "Top Level Sub B", 
        "parent_id": 2, 
        "path_info": [
          1, 
          2, 
          5
        ]
      }, 
      {
        "dept_id": null, 
        "id": 6, 
        "name": "Sub Level Bb", 
        "parent_id": 5, 
        "path_info": [
          1, 
          2, 
          5, 
          6
        ]
      }, 
      {
        "dept_id": null, 
        "id": 7, 
        "name": "Bb2", 
        "parent_id": 5, 
        "path_info": [
          1, 
          2, 
          5, 
          7
        ]
      }, 
      {
        "dept_id": null, 
        "id": 8, 
        "name": "Aa1", 
        "parent_id": 4, 
        "path_info": [
          1, 
          2, 
          4, 
          8
        ]
      }
    ]
  };  
  constructor() { }

  ngOnInit() {
    console.log(this.convertToObject(this.getNestedChildren(this.test["cat_list"],null)));
  }
  getNestedChildren(arr, parent) {
    var out = []
    for(var i in arr) {
        if(arr[i].parent_id == parent) {
            var children = this.getNestedChildren(arr, arr[i].id)

            if(children.length) {
                arr[i].children = children
            }
            out.push(arr[i])
        }
    }
    return out;
  }
  convertToObject(arr){
    console.log
    var obj={};
    arr.map((val,index)=>{
      console.log(val);

      if(val.hasOwnProperty("children"))
      {
        obj[val.name]=this.convertToObject(val.children);        
      }
      else{
        obj[val.name]={};
      }
    },this);
    return obj;
  }
}
