import { Component, OnInit } from '@angular/core';
import { element } from '@angular/core/src/render3/instructions';
import { FormGroup ,FormBuilder} from '@angular/forms';
import {DataSharingService} from '../services/dataShare/data-sharing.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {Http} from '@angular/http';
@Component({
  selector: 'app-edit-table-data',
  templateUrl: './edit-table-data.component.html',
  styleUrls: ['./edit-table-data.component.css']
})

export class EditTableDataComponent implements OnInit {
  editFormGroup:FormGroup;
  displayCategory:boolean=true;
  apiData:any={};
  catData:any={
    "id":1,
    "name":"root"
  };
  docId:number=null;
    
  constructor(public http:Http,public location:Location,public fb:FormBuilder,public router:Router,public dataShare:DataSharingService){
    this.editFormGroup=this.fb.group({
      "docDate":[],
      "category":[],
      "description":[],
      "customField":[],
      "byDate":[{value:"",disabled:true}],
      "alertDate":[],
      "filename":[{value:"",disabled:true}]
    })
    this.dataShare.getObservable().subscribe((value)=>{
      console.log(value);
      if(!value.hasOwnProperty("file_name"))
      this.router.navigate(["search"]);
      else
      {
        this.docId=value.id;
        this.apiData=value;
        this.catData.id=value.category_id;
        this.catData.name=value.cat_name;
        this.editFormGroup.controls.docDate.setValue(this.formatDate(value.doc_date));
        this.editFormGroup.controls.category.setValue(value.cat_name);
        this.editFormGroup.controls.description.setValue(value.description);
        this.editFormGroup.controls.customField.setValue(value.custom_tag);
        this.editFormGroup.controls.byDate.setValue(value.full_name+','+this.formatDate(value.up_date));
        this.editFormGroup.controls.alertDate.setValue(this.formatDate(value.alert_date));
        this.editFormGroup.controls.filename.setValue(value.file_name);
      }  
    })  
  }

  ngOnInit() {
  }

  selectedCategory(event){
    this.catData=event;
    this.editFormGroup.controls.category.setValue(this.catData.name);
  }

  onSubmit(value){
    var formData=new FormData();
    value.alertDate!=null?formData.append("alert_date",value.alertDate):formData.append("alert_date","");
    value.customField!=null?formData.append("custom_tag",value.customField):formData.append("custom_tag","");
    value.description!=null?formData.append("description",value.description):formData.append("description","");
    this.catData.id!=null?formData.append("category_id",this.catData.id):formData.append("category_id","");
    value.docDate!=null?formData.append("doc_date",value.docDate):formData.append("doc_date","");
    console.log(this.docId);
    this.http.put("archival/api/v1.0/archival_docs/"+this.docId,formData)
    .subscribe((data)=>{
      this.location.back();
    });
  }

  formatDate(date) {
    if(date==undefined || date==null)
    return "";
    date=date.split("/").reverse().join("/");
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour=''+d.getHours(),
        mins=''+d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 10) hour = '0' + hour;
    if (mins.length < 10) mins = '0' + mins;
    console.log([year, month, day].join('-')+"T"+hour+":"+mins);
    return [year, month, day].join('-')+"T"+hour+":"+mins;
}

}
