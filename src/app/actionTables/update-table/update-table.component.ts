import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Http,Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-table',
  templateUrl: './update-table.component.html',
  styleUrls: ['./update-table.component.css']
})
export class UpdateTableComponent implements OnInit {
  dataSource:any[];
  displayedColumns:string[]=['up_date','remarks'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public http:Http,public route:ActivatedRoute) { 
   this.getData();
  }

  getData(){
    this.route.params.subscribe((params)=>{
      this.http.get("archival/api/v1.0/archival_docs/"+params.docId+"/updates")
    .pipe(map((response:Response)=>response.json()))
    .subscribe((val)=>{
      console.log(val);
      this.dataSource=val["updates"];
      this.dataSource=this.dataSource.sort(function(a,b){
        if(new Date(a.up_date).getTime() < new Date(b.up_date).getTime() )
        return -1;
        else return 1;
      });
    });
    });
  }

  ngOnInit() {
  }

}
