import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadLogTableComponent } from './download-log-table.component';

describe('DownloadLogTableComponent', () => {
  let component: DownloadLogTableComponent;
  let fixture: ComponentFixture<DownloadLogTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadLogTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadLogTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
