import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ActionService } from '../../services/actionsService/action-service.service';

@Component({
  selector: 'app-download-log-table',
  templateUrl: './download-log-table.component.html',
  styleUrls: ['./download-log-table.component.css']
})
export class DownloadLogTableComponent implements OnInit {
  dataSource:any;
  displayedColumns:string[]=['user','date','version','filename'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public route:ActivatedRoute,public actions:ActionService ) {
    this.route.params.subscribe((param)=>{
      this.actions.downloadLog(param.docId).then((val)=>{
        this.dataSource=val["doc_logs"];
        console.log(val);
      });
    });
   }

  ngOnInit() {
    this.dataSource=new MatTableDataSource<any>(this.dataSource);
    this.dataSource.paginator=this.paginator;
  
  }

}
