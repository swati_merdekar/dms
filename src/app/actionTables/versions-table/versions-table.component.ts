import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Http ,Response} from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-versions-table',
  templateUrl: './versions-table.component.html',
  styleUrls: ['./versions-table.component.css']
})
export class VersionsTableComponent implements OnInit {
  dataSource:any;
  displayedColumns:string[]=['version_number','uploadByDate','check_in_remarks','version_file_name'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public http:Http,public route:ActivatedRoute) {
    this.getData();
   }  

   getData(){
    this.route.params.subscribe((params)=>{
      this.http.get("archival/api/v1.0/archival_docs/"+params.docId+"/versions")
    .pipe(map((response:Response)=>response.json()))
    .subscribe((val)=>{
      console.log(val);
      this.dataSource=val["doc_version_list"];
      this.dataSource=this.dataSource.sort(function(a,b){
        if(new Date(a.up_date).getTime() < new Date(b.up_date).getTime() )
        return -1;
        else return 1;
      });
    });
    });
   }

  ngOnInit() {
    this.dataSource=new MatTableDataSource<any>(this.dataSource);
    this.dataSource.paginator=this.paginator;
  }

}
