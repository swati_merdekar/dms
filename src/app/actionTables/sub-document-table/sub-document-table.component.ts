import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Http ,Response} from '@angular/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sub-document-table',
  templateUrl: './sub-document-table.component.html',
  styleUrls: ['./sub-document-table.component.css']
})
export class SubDocumentTableComponent implements OnInit {
  dataSource:any;
  displayedColumns:string[]=['description','uploadByDate','file_name'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(public http:Http,public route:ActivatedRoute) { 
    this.route.params.subscribe((param)=>{
    this.http.get("archival/api/v1.0/archival_docs/"+param.docId+"/subdocs")
    .pipe(map((response:Response)=>response.json()))
    .subscribe((val)=>{
      console.log(val);
      this.dataSource=val["sub_doc_list"];
      this.dataSource=this.dataSource.sort(function(a,b){
        if(new Date(a.up_date).getTime() < new Date(b.up_date).getTime() )
        return -1;
        else return 1;
      });
    });
  });
  }

  ngOnInit() {
    this.dataSource=new MatTableDataSource<any>(this.dataSource);
    this.dataSource.paginator=this.paginator;
  
  }

}
