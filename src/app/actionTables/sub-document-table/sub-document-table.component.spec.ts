import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDocumentTableComponent } from './sub-document-table.component';

describe('SubDocumentTableComponent', () => {
  let component: SubDocumentTableComponent;
  let fixture: ComponentFixture<SubDocumentTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubDocumentTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDocumentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
